import Firebase from 'firebase';

var config = {
    apiKey: "AIzaSyCnMT8Bkr8ZMoclLEjo362P_27diPwynGc",
    authDomain: "todo-19277.firebaseapp.com",
    databaseURL: "https://todo-19277.firebaseio.com",
    projectId: "todo-19277",
    storageBucket: "todo-19277.appspot.com",
    messagingSenderId: "591401538017"
};

const fb = Firebase.initializeApp(config);
const db = fb.database();

export default db;