import Vue from 'vue';
import VueFire from 'vuefire';
import App from './App.vue';

import Ripple from 'vue-ripple-directive';
Vue.directive('ripple', Ripple);

Vue.use(VueFire);

new Vue({
  el: '#app',
  render: h => h(App)
});
